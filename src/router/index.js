import Vue from 'vue'
import Router from 'vue-router'


import MainLayout from '../layouts/MainLayout'
import HomeLayout from '../layouts/HomeLayout'
import ExamCenterStaffLayout from '../layouts/ExamCenterStaffLayout'


import HomeLoginView from '../views/HomeLoginView'
import HomeVerificationView from '../views/HomeVerificationView'

import DashboardView from '../views/exams/DashboardView'
import NewExamView from '../views/exams/NewExamView'
import ExamDetailsView from '../views/exams/ExamDetailsView'
import ExamContentView from '../views/exams/ExamContentView'
import ExamPreviewView from '../views/exams/ExamPreviewView'
import ExamCollectionView from '../views/exams/ExamCollectionView'
import ExamGenerationView from '../views/exams/ExamGenerationView'

import PendingExams from '../views/exam-centre/PendingExams'
import CompletedExams from '../views/exam-centre/CompletedExams'
import PendingCompletedMenu from '../views/exam-centre/PendingCompletedMenu'
import ExamRecordMenuView from '../views/exam-centre/ExamRecordMenuView'
import ExamGenerationDetailsView from '../views/exam-centre/ExamGenerationDetailsView'

import ChairpersonDashboardMenu from '../views/chairperson/ChairpersonDashboardMenu'

import ExamSetting from '../views/chairperson/ExamSetting'
import ExamModeration from '../views/chairperson/ExamModeration'
import ExamGeneration from '../views/chairperson/ExamGeneration'
import ExamCollection from '../views/chairperson/ExamCollection'

Vue.use(Router)

export default new Router({
    mode: 'history',
	routes: [
		{
			path: '',
			component: HomeLayout,
			children: [
				{
					path: '',
					component: HomeLoginView,
					name: 'home'
				},

				{
					path: 'verification',
					component: HomeVerificationView
				}
			]
		},

		{
			path: '/exam-centre/home',
			component: MainLayout,
			children: [
				{
					path: '',
					component: PendingCompletedMenu,
					children: [
						{
							path: 'pending',
							component: PendingExams
						},

						{
							path: 'completed',
							component: CompletedExams
						}
					]
				}
			]
		},

		{
			path: '/exam-centre/exam',
			component: MainLayout,
			children: [
				{
					path: '',
					component: ExamRecordMenuView,
					children: [
						{
							path: 'preview',
							component: ExamPreviewView
						},
						{
							path: 'details',
							component: ExamGenerationDetailsView
						}
					]
				}
			]
		},

		{
			path: '/chairperson',
			component: MainLayout,
			children: [
				{
					path: 'setting',
					component: ExamSetting,
					name: 'chairpersonSetting'
				},

				{
					path: 'moderation',
					component: ExamModeration,
					name: 'chairpersonModeration'
				},

				{
					path: 'generation',
					component: ExamGeneration
				},

				{
					path: 'collection',
					component: ExamCollection
				}
			]
		},


		{
			path: '/exams',
			component: MainLayout,
			children: [
			    {
			        path: 'dashboard',
			        component: DashboardView,
			        name: 'exams-dashboard'
			    },

			    {
			        path: 'new',
			        component: NewExamView ,
			        children: [
			            {
			                path: '',
			                component: ExamDetailsView
			            },

			            {
			                path: 'details',
			                name: 'examDetails',
			                component: ExamDetailsView
			            },

			            {
			                path: 'content',
			                component: ExamContentView                    
			            },

			            {
			            	path: 'preview',
			            	component: ExamPreviewView
			            },

			            {
			            	path: 'collection',
			            	component: ExamCollectionView
			            },

			            {
			            	path: 'generation',
			            	component: ExamGenerationView
			            }
			        ]
			    }
			]
		}
	]
})
