import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'


Vue.use(Vuex);


const state = {
	user: null,
	courses: [],
	allExams: [],
	examContent: [],
	deletedExamItems: [],
	exam: null
}


const mutations = {
	updateUser: (state, user) => {
		state.user = user
	},

	setCourses: (state, courses) => {
		state.courses = courses
	},

	initExamContent: (state) => {
		state.examContent = []
	},

	getExamContent: (state, examId) => {
		//pulls exam content from API
		let url = "http://localhost:8084/v1/examination/" + examId + "/content"

		axios.get(url)
		.then(response => {
			var content = response.data.data.content
			state.examContent = content
		})
		.catch(error => {
			console.log(error)
		})

	},

	getExam: (state, examId) => {
		let url = "http://localhost:8084/v1/examination/" + examId

		axios.get(url)
		.then(response => {
			var exam = response.data.data.exam
			state.exam = exam
		})
		.catch(error => {
			console.log(error)
		})

	},
	
	setExam: (state, exam) => {
		state.exam = exam
	},

	setDeletedExamItems: (state, item) => {
		state.deletedExamItems.push(item)
	},

	refreshDeletedExamItems: (state) => {
		state.deletedExamItems = []
	},

	setAllExams: (state, exams) => {
		state.allExams = exams
	},

	updateModerationStatusOfItem: (state, details) => {
		let content = state.examContent

		for (var i = 0; i <= content.length - 1; i ++) {
			let currentItem = content[i]
			if (currentItem.id == details.itemId) {
				// alert("From store, updated moderation status of item")
				currentItem.moderationDetails = details
				break
			}
		}
	}
}


const actions = {
	setAuthenticatedUser: (state, authenticatedUser) => {
		state.commit('updateUser', authenticatedUser)
	},

	getExamContent: (state, examId) => {
		state.commit('getExamContent', examId)
	},

	addItemToExamContent: (state, item) => {
		state.commit('addItemToExamContent', item)
		state.commit('getExamContent', item.examId)
	},

	setExam: (state, exam) => {
		state.commit('setExam', exam)
	},

	getExam: (state, examId) => {
		state.commit('getExam', examId)
	},

	initExamContent: (state) => {
		state.commit('initExamContent')
	},

	setDeletedExamItems: (state, item) => {
		state.commit('setDeletedExamItems', item)
	}, 

	refreshDeletedExamItems: (state) => {
		state.commit('refreshDeletedExamItems')
	},

	setCourses: (state, courses) => {
		state.commit('setCourses', courses)
	},

	setAllExams: (state, exams) => {
		state.commit('setAllExams', exams)
	},

	updateModerationStatusOfItem: (state, details) => {
		state.commit('updateModerationStatusOfItem', details)
	}
}


const getters = {
	user: (state) => {
		return state.user
	},

	courses: (state) => {
		return state.courses
	},

	examContent: (state) => {
		return state.examContent
	},

	exam: (state) => {
		return state.exam
	},

	deletedExamItems: (state) => {
		return state.deletedExamItems
	},

	allExams: (state) => {
		return state.allExams
	}
}

export default new Vuex.Store({
	state,
	mutations,
	actions,
	getters
});