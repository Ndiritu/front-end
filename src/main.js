// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import store from './store';
import router from './router'
import VueQuillEditor from 'vue-quill-editor'


Vue.config.productionTip = false

window.$ = window.jQuery = require('jquery')
require('semantic-ui/dist/semantic.min.css')
require('semantic-ui/dist/semantic.min.js')
require('semantic-ui-calendar/dist/calendar.min.css')
require('semantic-ui-calendar/dist/calendar.min.js')

require('quill/dist/quill.core.css')
require('quill/dist/quill.snow.css')
require('quill/dist/quill.bubble.css')

require('./assets/tablesort.js')

Vue.use(VueQuillEditor)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
})
